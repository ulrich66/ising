# Visual Ising Model #

A standard Metropolis simulation of the Ising model in D=2.

The animation shows on the screen the evolution of the spin field.

### How to run it ###

* Make the script executable.
	- The hashtag is
	\#!/usr/bin/env python3
	Change accordingly to what you need, in case.
* ./ising\_anim.py --help
	- Show you the options.
	Basically all you need is to declare lattice linear size.
	Try ./ising\_anim.py -L 128

### Dependencies ###

The code has been tested with python 3.6.1

* numpy
* matplotlib
* docopt

### Contribution guidelines ###

Contributions are not currentrly planned.

### Contacts ###

For any question please contact ulrichookii@gmail.com