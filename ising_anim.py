#!/usr/bin/env python3

"""
	ising_anim.py - Ising model on a square lattice in D=2, v. 1.1

  Usage:
    ./ising_anim.py (-h | --help)
    ./ising_anim.py --version 
    ./ising_anim.py -L <L> [-b <BETA>] [-n <NSWEEP>] [-s <START>]

  Arguments:

  Options:
    -h --help    Display this help and exit
    --version    Display version and exit
    -L <L>       Lattice size
    -b <BETA>    Inverse temperature; if negative default to critical value (see code) [default: -1.0]
    -n <NSWEEP>  Number of MC sweeps [default: 1000]
    -s <START>   Start; c for 'cold', h for 'hot' [default: h]
"""

import math
import random
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import time
import docopt

#----------------------------------------------------------
# parse options
#----------------------------------------------------------

BETAC = math.log(1.0+math.sqrt(2.0))/2.0 # infinite volume critical value

argv = docopt.docopt(__doc__, version="ising_anim.py v. 1.1")

L       = int(argv["-L"])
BETA    = float(argv["-b"])
NSWEEP  = int(argv["-n"])
START   = argv["-s"]
if BETA < 0.0:
	BETA = BETAC

m2beta = -2.0*BETA
nWrite = 100
nPlot  = 10

#----------------------------------------------------------
# define and init field
#----------------------------------------------------------
spin = np.ones((L,L), dtype=int)
if START == 'h':
	print("Hot Start")
	for ix in range(L):
		for iy in range(L):
			if random.random() < 0.5:
				spin[ix][iy] = -1
else:
	print("Cold Start")

#----------------------------------------------------------
# define and init geometry; Periodic Boundary Conditions
#----------------------------------------------------------
up = np.zeros(L, dtype=int)
dn = np.zeros(L, dtype=int)
for ix in range(L):
	up[ix] = ix + 1 if ix < L-1 else 0
	dn[ix] = ix - 1 if ix > 0   else (L-1)

#----------------------------------------------------------
# a single sweep through the lattice
#----------------------------------------------------------
def sweep():
	for ix in range(L):
		for iy in range(L):
			sigma = spin[up[ix],iy] + spin[dn[ix],iy] + spin[ix,up[iy]] + spin[ix,dn[iy]]
			minusBetaDeltaH = m2beta*spin[ix,iy]*sigma
			if math.log(1.0-random.random()) < minusBetaDeltaH:
				spin[ix][iy] = -spin[ix][iy]

#----------------------------------------------------------
# Animation setup
#----------------------------------------------------------
markerSize = 400.0/L # that's an higly "empirical" formula, it almost work for me
fig = plt.figure(figsize=(8,8), facecolor='#000000')
fig.subplots_adjust(left=0.1, right=0.9, bottom=0.1, top=0.9)
ax = fig.add_subplot(111, aspect='equal', autoscale_on=False, xlim=(0,L), ylim=(0,L), facecolor='#ffffff')
# I know, i'm cheating, but "blu" has the same number of letters of "red" :) (and it's not even english)
blu, = ax.plot([], [], 'yo', ms=markerSize)
red, = ax.plot([], [], 'ro', ms=markerSize)

#----------------------------------------------------------
# This function defines the "background" of each frame
#----------------------------------------------------------
def init_anim():
	blu.set_data([],[])
	red.set_data([],[])
	return blu, red

#----------------------------------------------------------
# Main animation loop
#----------------------------------------------------------
def animate(iframe):
	i = iframe + 1

	# Next "if" has the effect that the image is frozen to
	# initial conditions for a while
	if iframe > 10:
		sweep()

	if i % nWrite == 0:
		print("Sweep %8d / %8d done" % (i, NSWEEP))

	blu_x = []
	blu_y = []
	red_x = []
	red_y = []
	for ix in range(L):
		for iy in range(L):
			if spin[ix][iy] == 1:
				blu_x.append(ix+0.5)
				blu_y.append(iy+0.5)
			else:
				red_x.append(ix+0.5)
				red_y.append(iy+0.5)
	blu.set_data(blu_x,blu_y)
	red.set_data(red_x,red_y)
# for small lattice a bit sleeping is needed
#	time.sleep(0.1)
	return blu, red

#----------------------------------------------------------
# Let's start the animation
#----------------------------------------------------------

interval = 1
ani = animation.FuncAnimation(fig, animate, frames=NSWEEP, repeat=False,
	                            interval=interval, blit=True, init_func=init_anim)

# TODO add an option to save or see the animation
#ani.save('basic_animation.mp4', fps=30, extra_args=['-vcodec', 'libx264'])
plt.show()


